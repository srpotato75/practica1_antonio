﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks.Dataflow;

namespace practica1_Antonio
{
    class Program
    {
        static void Main(string[] args)
        {
            //ejercicio1();
            //ejercicio2();
            //ejercicio3();
            //ejercicio5();
            //ejercicio6();
            ejercicio7();
        }

        public static void ejercicio1()
        {
            double nota, aprobados, sobresalientes;
            int total;

            aprobados = 0;
            sobresalientes = 0;
            total = 0;

            while (total<10)
            {
                Console.WriteLine("Escribe la nota del alumno");
                nota = double.Parse(Console.ReadLine());
                total++;

                if (nota >= 5)
                {
                    aprobados++;
                    if (nota >= 9)
                    {
                        sobresalientes++;
                    }
                }              
            }

            Console.WriteLine("El numero de aprobados es: " + aprobados + "El numero de sobresalientes es: " + sobresalientes);
        }

        public static void ejercicio2()
        {
            DateTime date1 = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("La fecha actual es: " + date1);
            date1 = date1.AddDays(90);
            Console.WriteLine("La fecha del pago es: " + date1);
        }

        public static void ejercicio3()
        {
            int i;
            int[] num = new int[10];
            num[0] = 1;
            num[1] = 2;
            num[2] = 3;
            num[3] = 4;
            num[4] = 5;
            num[5] = 6;
            num[6] = 7;
            num[7] = 8;
            num[8] = 9;
            num[9] = 10;
            Console.WriteLine("Bucle for");
            for ( i = 0; i < 10; i++)
            {
                Console.WriteLine("El numero es " + num[i]);
            }
            while (i > 0)
            {
                i--;
                Console.WriteLine("El numero es " + num[i]);
            }
        }

        public static void ejercicio5()
        {
            double n1, n2, n3, n4, n5, n6, n7, n8, n9, n10;
            double max;
            double max2;
            n1 = Double.Parse(Console.ReadLine());
            n2 = Double.Parse(Console.ReadLine());
            n3 = Double.Parse(Console.ReadLine());
            n4 = Double.Parse(Console.ReadLine());
            n5 = Double.Parse(Console.ReadLine());
            n6 = Double.Parse(Console.ReadLine());
            n7 = Double.Parse(Console.ReadLine());
            n8 = Double.Parse(Console.ReadLine());
            n9 = Double.Parse(Console.ReadLine());
            n10 = Double.Parse(Console.ReadLine());

            max = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10)))))))));
            Console.WriteLine("El mayor es: " + max);
            if (max == n1 )
            {
                max2 = Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10))))))));
                Console.WriteLine("El segundo mayor es: " + max2);
            } 

            if (max == n2 )
            {
                max2 = Math.Max(n1, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10))))))));
                Console.WriteLine("El segundo mayor es: " + max2);
            } 
            if (max == n3 )
            {
                max2 = Math.Max(n1, Math.Max(n2, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10))))))));
                Console.WriteLine("El segundo mayor es: " + max2);
            }
            if (max == n4)
            {
                max2 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10))))))));
                Console.WriteLine("El segundo mayor es: " + max2);
            }
            if (max == n5)
            {
                max2 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10))))))));
                Console.WriteLine("El segundo mayor es: " + max2);
            }
            if (max == n6)
            {
                max2 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10))))))));
                Console.WriteLine("El segundo mayor es: " + max2);
            }
            if (max == n7)
            {
                max2 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n8, Math.Max(n9, n10))))))));
                Console.WriteLine("El segundo mayor es: " + max2);
            }
            if (max == n8)
            {
                max2 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n9, n10))))))));
                Console.WriteLine("El segundo mayor es: " + max2);
            }
            if (max == n9)
            {
                max2 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, n10))))))));
                Console.WriteLine("El segundo mayor es: " + max2);
            }
            if (max == n3)
            {
                max2 = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, n9))))))));
                Console.WriteLine("El segundo mayor es: " + max2);
            }
        }

        public static void ejercicio6()
        {
            double n1, n2, n3, n4, n5, n6, n7, n8, n9, n10; 
            int diferentes = 10;

            n1 = Double.Parse(Console.ReadLine());
            n2 = Double.Parse(Console.ReadLine());
            n3 = Double.Parse(Console.ReadLine());
            n4 = Double.Parse(Console.ReadLine());
            n5 = Double.Parse(Console.ReadLine());
            n6 = Double.Parse(Console.ReadLine());
            n7 = Double.Parse(Console.ReadLine());
            n8 = Double.Parse(Console.ReadLine());
            n9 = Double.Parse(Console.ReadLine());
            n10 = Double.Parse(Console.ReadLine());

            if (n1 == n2 || n1 == n3 || n1 == n4 || n1 == n5 || n1 == n6 || n1 == n7 || n1 == n8 || n1 == n9 || n1 == n10)
            {
                diferentes--;
            }
            if (n2 == n1 | n2 == n3 | n2 == n4 | n2 == n5 | n2 == n6 | n2 == n7 | n2 == n8 | n2 == n9 | n2 == n10)
            {
                diferentes--;
            }
            if (n3 == n2 | n1 == n3 | n3 == n4 | n3 == n5 | n3 == n6 | n3 == n7 | n3 == n8 | n3 == n9 | n3 == n10)
            {
                diferentes--;
            }
            if (n4 == n2 | n4 == n3 | n1 == n4 | n4 == n5 | n4 == n6 | n4 == n7 | n4 == n8 | n4 == n9 | n4 == n10)
            {
                diferentes--;
            }
            if (n5 == n2 | n5 == n3 | n5 == n4 | n1 == n5 | n5 == n6 | n5 == n7 | n5 == n8 | n5 == n9 | n5 == n10)
            {
                diferentes--;
            }
            if (n6 == n2 | n6 == n3 | n6 == n4 | n6 == n5 | n1 == n6 | n6 == n7 | n6 == n8 | n6 == n9 | n6 == n10)
            {
                diferentes--;
            }
            if (n7 == n2 | n7 == n3 | n7 == n4 | n7 == n5 | n7 == n6 | n1 == n7 | n7 == n8 | n7 == n9 | n7 == n10)
            {
                diferentes--;
            }
            if (n8 == n2 | n8 == n3 | n8 == n4 | n8 == n5 | n8 == n6 | n8 == n7 | n1 == n8 | n8 == n9 | n8 == n10)
            {
                diferentes--;
            }
            if (n9 == n2 | n9 == n3 | n9 == n4 | n9 == n5 | n9 == n6 | n9 == n7 | n9 == n8 | n1 == n9 | n9 == n10)
            {
                diferentes--;
            }
            if (n10 == n2 | n10 == n3 | n10 == n4 | n10 == n5 | n10 == n6 | n10 == n7 | n10 == n8 | n10 == n9 | n1 == n10)
            {
                diferentes--;

            }
                Console.WriteLine("Hay estos numeros diferentes " + (diferentes));

        }

        public static void ejercicio7()
        {
            
            int[] notas = new int[10];
            double media; 
            for (int i = 0; i < notas.Length; i++)
            {              
                Console.WriteLine("Escribe una nota: ");
                notas[i] = int.Parse(Console.ReadLine());
                
            }
            if (notas.Max() - notas.Min() >= 3)
            {
                Console.WriteLine("La nota mayor: " + notas.Max() + " ,y la nota menor: "+ notas.Min() + " ,se han quitado de la media");
                media = notas.Sum() - notas.Max();
                media = media - notas.Min();
                media = media / 8;
                Console.WriteLine("La media es: " + media);
            }
            else
            {
                media = notas.Sum() / 10;
                Console.WriteLine("La media es: " + media);
            }
        }

    }

}

